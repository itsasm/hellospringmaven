package hello;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class MyRestControllerTest {

    private static MyRestController greetingController;

    @BeforeClass
    public static void setUp() {
        greetingController = new MyRestController();
    }

    @Test
    public void shouldCheckInstanceOfGreetingController() {
        assertNotNull(greetingController);
    }

    @Test
    public void shouldCallDetailsMethodInGreetingController() {
        assertEquals("Welcome : Ajay Singh Meena", greetingController.welcome());
    }
}
