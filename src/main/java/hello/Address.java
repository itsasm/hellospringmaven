package hello;

public class Address {

    private final int Hno;
    private final String street;
    private final int pincode;

    public Address(int Hno, String street, int pincode){
        this.Hno = Hno;
        this.street = street;
        this.pincode = pincode;
    }

    public int getHno(){
        return Hno;
    }

    public String getStreet(){
        return  street;
    }

    public int getPincode(){
        return pincode;
    }
}
