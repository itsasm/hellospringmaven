package hello;

public class Employee {

    private final long id;
    private final String name;
    private final Address address;

    public Employee(long id, String name, Address address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }

    public String getName(){
        return name;
    }
}