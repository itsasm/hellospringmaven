package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class MyRestController {

    private static final String template = "%s";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/")
    public String welcome(){
        return "Welcome : Ajay Singh Meena";
    }

    @RequestMapping("/details")
    public Employee greeting(@RequestParam(value="name", defaultValue="World") String name,
                             @RequestParam(value="Hno", defaultValue="44") int Hno,
                             @RequestParam(value="street", defaultValue="Boriya Khamkheda") String street,
                             @RequestParam(value="pincode", defaultValue="464226") int pincode) {

        Address address = new Address(Hno, street, pincode);
        Employee employee = new Employee(counter.incrementAndGet(),
                String.format(template, name), address);

        return employee;
    }
}